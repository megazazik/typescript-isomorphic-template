import * as React from 'react';
import warningImg  from './img/warning.png';

export default () => (<img src={warningImg} />);