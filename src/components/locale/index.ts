import { createContext} from 'react';
import { Language } from 'modules/lang';

export const { Provider, Consumer } = createContext(Language.Rus);