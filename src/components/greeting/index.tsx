import * as React from "react";
import styles from './styles.css';
import addLabels, { ILocaleProps } from 'components/labels';

export interface IProps extends ILocaleProps {
	name: string;
}

export function Greeting(props: IProps) {
	return (
		<div className={styles.greeting}>
			{props.labels.start}<span className={styles.name}>{props.name}</span>{props.labels.end}
		</div>
	);
}

export default addLabels((locale) => require(`./lang/${locale}.json`))(Greeting);