import * as React from 'react';
import * as styles from './styles/index.less';

export enum Types {
	Warning,
	Info
}

const MESSAGE_CLASS_NAME = 'message';
const WARNING_CLASS_NAME = 'warning';
const INFO_CLASS_NAME = 'info';

export default function Message (props: {message: string, type?: Types}) {
	let className = styles[MESSAGE_CLASS_NAME] + ' ';
	switch (props.type) {
		case Types.Info:
			className += styles[INFO_CLASS_NAME];
			break;
		default:
			className += styles[WARNING_CLASS_NAME];
	}

	return (
		<div className={className}>{props.message}</div>
	);
}