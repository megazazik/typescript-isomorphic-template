import * as React from 'react';
import { Consumer } from '../locale';

export interface ILocaleProps {
	labels: any
}

export default function createContextLabels(importLabels: (locale: import('modules/lang').Language) => object) {
	return function contextLabelsHoc<P extends ILocaleProps>(Component: React.ReactType<P>) {
		return function ContextLabels(props: Omit<P, 'labels'>) {
			return (
				<Consumer>
					{locale => <Component {...props} labels={importLabels(locale)}/>}
				</Consumer>
			);
		}
	}
}