import * as React from 'react';
import Message, { Types as MessageTypes } from '../message';
import Greeting from '../greeting';
import Image from '../warningImage';
import HomeImage from '../homeImage';
import CopyRight from '../copyright';
import addLabels, { ILocaleProps } from 'components/labels';
import { IComponentProps } from 'app';

export interface IProps extends ILocaleProps, Partial<IComponentProps> {}

export class PageView extends React.Component<IProps> {
	static defaultProps: Partial<IProps> = {
		onLocaleChange: () => {},
	}

	render() {
		return (
			<div>
				<Message message={this.props.labels.warging} />
				<Message message={this.props.labels.info} type={MessageTypes.Info} />
				<Greeting name={this.props.labels.name} />
				<Image />
				<br/>
				<HomeImage />
				<br/>
				<button onClick={this.props.onLocaleChange}>{this.props.labels.changeLanguage}</button>
				<br/>
				<CopyRight />
			</div>
		);
	}
}

export default addLabels((locale) => require(`./lang/${locale}.json`))(PageView);