import * as React from 'react';
import { storiesOf } from '@storybook/react';
import Composite from '../';

storiesOf('CompositeComponent', module)
	.add('Common', () => (
		<Composite />
	));