const React = require('react');
const { default: addLabels } = require('components/labels');

const CopyRight = (props) => {
	return (
		<div>{props.labels.text}</div>
	);
}

module.exports = addLabels((locale) => require(`./lang/${locale}.json`))(CopyRight);