import * as React from 'react';
import { storiesOf } from '@storybook/react';
import Copyright from './';

storiesOf('Copyright', module)
	.add('Common', () => (
		<Copyright />
	));