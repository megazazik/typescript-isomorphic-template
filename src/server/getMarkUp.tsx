import * as React from 'react';
import { renderToString } from 'react-dom/server';
import PageView from 'components/composite';
import {Language} from 'modules/lang';
import { Provider } from 'components/locale';

export default (): string => {
	const currentLanguage = Language.Rus;

	const html = renderToString(
		<Provider value={currentLanguage}>
			<PageView />
		</Provider>
	);
	
	return `
<!doctype html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>Typescript application template</title>
		<link rel="stylesheet" href="/static/bundle.css" />
	</head>
	<body>
		<div id="content">${html}</div>
		<script>
			window.__language__ = "${currentLanguage}";
		</script>
		${Object.keys(Language).map(
			(key) => Language[key] === currentLanguage
				? `<script src="/static/bundle.${currentLanguage}.js"></script>`
				: `<script>
					(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["bundle.${Language[key]}"],{}]);
				</script>`
		).join('')}
		<script src="/static/bundle.js"></script>
	</body>
</html>`;
};