import { render } from 'react-dom';
import * as React from 'react';
import { Language } from 'modules/lang';
import { Provider } from 'components/locale';
import loadScript from 'modules/loadScript';

export interface IComponentProps {
	onLocaleChange: () => void;
}

const initLanguage = window['__language__'] as Language;

export default function startApplication(Component: React.ReactType<IComponentProps>) {
	let currentLanguage = initLanguage;
	let languageLoaded = false;

	const renderPage = () => {
		render(
			(
				<Provider value={currentLanguage}>
					<Component onLocaleChange={onLocaleChange} />
				</Provider>
			),
			document.getElementById('content')
		);
	}

	const onLocaleChange = () => {
		let language = currentLanguage === Language.Rus ? Language.Eng : Language.Rus;
		if ((initLanguage !== language) && !languageLoaded) {
			loadScript(`/static/bundle.${language}.js`, () => {
				languageLoaded = true;
				currentLanguage = language;
				renderPage();
			});
		} else {
			currentLanguage = language;
			renderPage();
		}
	}

	renderPage();
}