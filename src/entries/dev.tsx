import PageView from 'components/composite';
import { hot } from 'react-hot-loader';
import runApp from 'app';

const HotComponent = hot(module)(PageView);

runApp(HotComponent);
